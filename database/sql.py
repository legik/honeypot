from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os


class IP(declarative_base()):
    __tablename__ = 'ip'

    id = Column(Integer, primary_key=True)
    address = Column(String(45), unique=True, nullable=False)

    def __repr__(self):
        return '<Ip %r>' % self.address


class DB:
    def __init__(self):
        engine = create_engine('postgresql://{}/ip'.format(os.environ['SQL_CREDS']), echo=False)
        self.Session = sessionmaker(bind=engine)

    def get_session(self):
        return self.Session()