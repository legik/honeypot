
from application import ExampleCollector


def test_collector():
    """Test for test collector"""
    d = ExampleCollector()
    assert d.run() == 5
