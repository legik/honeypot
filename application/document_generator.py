
from yattag import Doc
from time import gmtime, strftime

class DocumentGenerator:
    """class DocumentGenerator provides generate report in HTML format"""
    def __init__(self, ip, port, full_metrics, sum_metrics):
        self.full_metrics = full_metrics
        self.sum_metrics = sum_metrics
        self.ip = ip
        self.port = port
        pass

    def make_report(self):
        """This method generates HTML document"""
        name = '{}_{}_report_{}.html'.format(self.ip, self.port,
                                             strftime("%Y-%m-%d_%H:%M:%S", gmtime()))
        file = open('report/'+name, 'w')

        doc, tag, text = Doc().tagtext()

        doc.asis('<!DOCTYPE html>')
        with tag('html'):
            with tag('head'):
                with tag('style'):
                    text('table.b, th.b, td.b {border: 1px solid black; border-collapse: collapse;}')
                    text('table.wb, th.wb, td.wb {border: 0px solid black; border-collapse: collapse;}')
                    text('p.rbrl {display: inline-block;}')
            with tag('body'):
                with tag('div', align='center'):
                    doc.stag('img', src='logo.png', width='300', height='200')
                    doc.stag('br')
                    with tag('p', klass='rbrl'):
                        text('Probability of honeypot is ')

                    prob_honeypot = round(self.sum_metrics['Base'], 2) * 100
                    prob_style = 'color: {}'.format('green' if prob_honeypot <= 80 else 'red')

                    with tag('p', klass='rbrl', style='margin-bottom: 0px; font-size:160%; {}'.format(prob_style)):
                        text(' {}% '.format(prob_honeypot))

                    doc.stag('br')

                    if self.full_metrics['Base']['prev_honey'] != 0.0:
                        with tag('i', klass='rbrl', style='margin-top: 0px;'):
                            text('(There was honeypot on this ip address)')

                    doc.stag('br')
                    doc.stag('br')

                    with tag('table', klass='b'):
                        with tag('tr', klass='b'):
                            with tag('th', klass='b'):
                                text('Metric Name')
                            with tag('th', klass='b'):
                                text('Valuation')
                        for metrics, value in self.sum_metrics.items():
                            if metrics == 'Summary':
                                continue

                            with tag('tr', klass='b'):
                                with tag('td', klass='b'):
                                    text(metrics)
                                with tag('td', klass='b'):
                                    text(round(value, 2))
                    doc.stag('br')
                    doc.stag('br')

                    with tag('table', klass='wb'):
                        for collector, metrics in self.full_metrics.items():
                            with tag('td', klass='wb', valign='top'):
                                with tag('table', klass='b'):
                                    with tag('tr', klass='b'):
                                        with tag('th', klass='b'):
                                            text('Metric Name')
                                        with tag('th', klass='b'):
                                            text('Valuation')
                                    for metrics_name, value in metrics.items():
                                        if metrics_name == 'prev_honey':
                                            continue

                                        with tag('tr', klass='b'):
                                            with tag('td', klass='b'):
                                                text(metrics_name)
                                            with tag('td', klass='b'):
                                                if value != 0:
                                                    text('yes')
                                                else:
                                                    text('no')

        file.write(doc.getvalue())
        file.close()
