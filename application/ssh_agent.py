
import time
import logging

import paramiko
import re
from paramiko import SSHException
from paramiko.ssh_exception import NoValidConnectionsError, AuthenticationException

module_logger = logging.getLogger('Application.SshAgent')

class SshAgent:
    """
        class SshAgent provide communication with server over SSH protocol
    """

    def __init__(self, serv_addr, port, user, pswd):
        self.client = None
        self.remote_conn = None

        self.serv_addr = serv_addr
        self.port = port
        self.user = user
        self.pswd = pswd

    def connect(self):
        return self._connect_by_password(self.serv_addr, self.port, self.user, self.pswd)

    def close(self):
        """This method provides close ssh connection"""
        self.client.close()

    def exec_command(self, command):
        """This method sends command to server and wait response"""
        self._send_input_to_server(command)

        return self._receive_output_from_server()

    def _connect_by_password(self, serv_addr, port, user, pswd):
        """This method connects to server"""
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

        try:
            client.connect(hostname=serv_addr, port=port, username=user, password=pswd)
        except NoValidConnectionsError as e:
            raise
        except AuthenticationException as e:
            return False

        remote_conn = client.invoke_shell()

        self.remote_conn = remote_conn
        self.client = client

        self._receive_output_from_server()

        return True

    def _receive_output_from_server(self):
        """This method receives response from server"""
        time.sleep(1)

        raw_output = self.remote_conn.recv(65535)

        output = SshAgent._delete_cursor(raw_output.decode('UTF-8'))

        rows = output.split('\r\n')

        if len(rows) == 0:
            module_logger.debug('Received message is empty')
            return

        rows = rows[1:-2]

        for row in rows:
            module_logger.debug('{}'.format(row))

        return rows

    def _delete_cursor(buff):
        """This method deletes cursor from ouput of terminal"""
        return re.sub('\x1b\[[0-9]{1}[a-z]{1}', '', buff)

    def _send_input_to_server(self, command):
        """This method send command to server"""
        self.remote_conn.send('{}\r\n'.format(command))
