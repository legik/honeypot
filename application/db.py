from database.sql import IP, DB


class Database:
    """ class Database provide access to the database """
    def __init__(self):
        self.table = IP.__table__
        db = DB()
        self.session = db.get_session()
        pass

    def check_ip(self, ip):
        """ check if IP address in the database or not"""
        result = self.session.query(IP).filter_by(address=ip).first()
        if not result:
            return 0
        else:
            return 1

    def save_db(self):
        """ save database state"""
        self.session.commit()

    def add_ip(self, ip):
        """ add IP address to the database """
        if self.check_ip(ip):
            print('{} already in the database'.format(ip))
            return 0
        else:
            honeypot_ip = IP(address=ip)
            self.session.add(honeypot_ip)
            self.save_db()
            print('{} added to honeypots IP database'.format(ip))
            return 1
