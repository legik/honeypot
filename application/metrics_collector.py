import logging

from paramiko.ssh_exception import NoValidConnectionsError

from .base_collector import BaseCollector
from .math_model import MathModel

from .db import Database
from .document_generator import DocumentGenerator


class MetricsCollector:
    """MetricsCollector class provides run different
       collectors for calculating metrics"""

    def __init__(self, host):
        self.honey_host = host[0]
        self.collectors = [BaseCollector(host)]

    def add_honeypot(self):
        database = Database()
        database.add_ip(self.honey_host)

    def run(self):
        """This method provides run calculating metrics"""
        logging.info('Start metric collector')
        result = {}

        for collector in self.collectors:
            try:
                metrics = collector.run()
            except NoValidConnectionsError as e:
                print(e)
                continue

            result[collector.get_name()] = metrics

        print('Metrics:')
        print(result)

        model = MathModel(result)
        calcs = model.calculate()

        if calcs['Base'] > 0.8:
            self.add_honeypot()

        print('Report:')
        print(calcs)

        doc = DocumentGenerator(self.collectors[0].host, self.collectors[0].port, result, calcs)
        doc.make_report()
