from paramiko.ssh_exception import NoValidConnectionsError

import random
import string
import requests
import time
import xmltodict
import nmap
import os

from .collector import Collector
from .ssh_agent import SshAgent
from application import db

VIRUSTOTAL_KEY = os.environ['VT_KEY']

openssh_version = ['4.2p1 Debian 7ubuntu3.1', '4.3', '4.6', '5.1p1 Debian 5', '5.1p1 FreeBSD  20080901',
                   '5.3p1 Debian 3ubuntu5', '5.3p1 Debian 3ubuntu6', '5.3p1 Debian 3ubuntu7', '5.5p1 Debian 6',
                   '5.5p1 Debian 6+squeeze1', '5.5p1 Debian 6+squeeze2', '5.8p2_hpn13v11 FreeBSD 20110503',
                   '5.9p1 Debian 5ubuntu1', '6.0p1 Debian 4+deb7u2', '5.9',
                   'SSH-1.99-Sun_SSH_1.1', 'SSH-2.0-OpenSSH_4.2p1 Debian-7ubuntu3.1',
                   'SSH-2.0-OpenSSH_5.1p1 Debian-5', '5.1p1 FreeBSD-20080901',
                   '5.3p1 Debian-3ubuntu5', '5.3p1 Debian-3ubuntu6', '5.3p1 Debian-3ubuntu7',
                   '5.5p1 Debian-6', '5.5p1 Debian-6+squeeze1', '5.5p1 Debian-6+squeeze2',
                   '5.8p2_hpn13v11 FreeBSD-20110503', '5.9p1 Debian-5ubuntu1', 
                   ]


class BaseCollector(Collector):
    """class BaseCollector provides calculating base metrics"""

    def __init__(self, host):
        super().__init__(host)
        self.name = 'Base'
        self.result = {}

    def run(self):
        self.ping_ttl()
        self.create_file_metrics()
        self.cd_home()
        self.login_pass()
        self.virustotal_url_check()
        self.previous_honeypot()
        self.check_return_val_of_command()
        self.check_error_stream()
        self.check_mkdir_with_flag()
        self.check_inode_number()
        self.check_loop_construction()
        self.check_wget_help()
        self.open_ports()
        self.openssh_ver()
        self.uname_check()

        return self.result

    def create_file_metrics(self):
        """ Check file and check exist this file after ssh disconnect """
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        ssh.exec_command('touch file')
        ssh.close()
        ssh.connect()

        out = ssh.exec_command('ls')
        print(out)
        res = 1

        if len(out) != 0:
            for file_name in out[0].split(' '):
                if file_name == 'file':
                    res = 0
                    break

        self.result["create_file"] = res

        ssh.close()

    def ping_ttl(self):
        """ ping check to localhost or not existing domain names"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        name = 'yourhoneypotisnotgood'

        ttl = ssh.exec_command('ping {}'.format(name))
        if ttl[0] == 'ping: yourhoneypotisnotgood: Name or service not known' or ttl[0] == '-bash: ping: command not found':
            self.result["ping"] = 0
        else:
            self.result["ping"] = 1
        ssh.close()

    def cd_home(self):
        """ check cd to home directory"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        out = ssh.exec_command('cd ~/')
        if len(out) > 0 and 'No such file or directory' in out[0]:
            self.result["cd"] = 1
        else:
            self.result["cd"] = 0
        ssh.close()

    def login_pass(self):
        """Check predefined login password pairs"""
        res = 0

        with open('passwords.xml') as ps:
            dic = xmltodict.parse(ps.read())

            for pair in dic['logpass']['pair']:
                ssh = SshAgent(self.host, self.port, str(pair['login']), str(pair['pass']))
                if ssh.connect() is not True:
                    continue
                else:
                    res = 1
                    ssh.close()
                    break

        self.result["logpass"] = res

    def virustotal_url_check(self):
        """ VirusTotal url metric """
        headers = {
            "Accept-Encoding": "gzip, deflate",
            "User-Agent": "gzip,  HoneyPot checker"
        }

        ssh = self.create_ssh_agent()
        if ssh.connect() is not True:
            return self.result

        url = '{}/{}'.format(os.environ['HOSTING'],self.random_word(15))
        ssh.exec_command('curl {}'.format(url))

        time.sleep(5)
        params = {'apikey': VIRUSTOTAL_KEY, 'resource': url}
        response = requests.post('https://www.virustotal.com/vtapi/v2/url/report',
                                 params=params, headers=headers)
        json_response = response.json()

        if json_response['response_code'] == 1:
            self.result['VT_url'] = 1
        else:
            self.result['VT_url'] = 0

    def previous_honeypot(self):
        database = db.Database()

        if database.check_ip(self.host):
            self.result["prev_honey"] = 1
        else:
            self.result["prev_honey"] = 0

    def check_return_val_of_command(self):
        """Check return value of program"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        ssh.exec_command('cd egregergegeg')
        out = ssh.exec_command('echo $?')

        if out[0] == '1':
            self.result['return_value_cmd'] = 0
        else:
            self.result['return_value_cmd'] = 1

        ssh.close()

    def check_error_stream(self):
        """Check error stream"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        ssh.exec_command('cd egregergegeg 2> error.log')
        out = ssh.exec_command('[ -f error.log ] && echo Found || echo Not found')

        if out[0] != 'Found':
            self.result['error_stream'] = 1
        else:
            out = ssh.exec_command('cat error.log')
            print(out)

            if len(out) == 0 or ('cat' in out[0] and 'such file or directory' in out[0]):
                self.result['error_stream'] = 1
            else:
                self.result['error_stream'] = 0

        ssh.close()

    def check_mkdir_with_flag(self):
        """Check command mkdir with flag"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        ssh.exec_command('mkdir -p f1/f2')
        ssh.exec_command('\x03') # CTRL+C
        out = ssh.exec_command('ls')

        if out[0].strip() == '-p':
            self.result['mkdir_with_flag'] = 1
        else:
            self.result['mkdir_with_flag'] = 0

        ssh.close()

    def check_inode_number(self):
        """Check inode number of file and folder"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        out = ssh.exec_command('ls -il /')
        ssh.close()

        row = out[1].split(' ')
        row = list(filter(None, row))

        if len(row) != 10:
            self.result['inode'] = 1
            return

        # check if element with index 0 has integer type
        try:
            inode = int(row[0])
            self.result['inode'] = 0
        except Exception as e:
            self.result['inode'] = 1

    def check_loop_construction(self):
        """Check inode number of file and folder"""
        ssh = self.create_ssh_agent()

        if ssh.connect() is not True:
            return

        out = ssh.exec_command('for i in {1..5}; do echo kxe; done')
        ssh.close()

        if len(out) != 5:
            self.result['loop'] = 1
        else:
            number_kxe = 0
            for row in out:
                if row == 'kxe':
                    number_kxe += 1

            if number_kxe == 5:
                self.result['loop'] = 0
            else:
                self.result['loop'] = 1

    def check_wget_help(self):
        """Check wget --help command"""
        ssh = self.create_ssh_agent()
        res = 0

        if ssh.connect() is not True:
            return

        out = ssh.exec_command('wget')

        if len(out) > 0 and "wget --help" in str(out):
            out = ssh.exec_command('wget --help')
            if len(out) > 0 and 'Unrecognized option' == out[0]:
                res = 1

        ssh.close()
        self.result['wget'] = res

    def open_ports(self):
        """ Check number of opened ports """
        nm = nmap.PortScanner()
        nm.scan(hosts=self.host, arguments='-Pn')
        # TODO: choose right coeff.
        if len(nm[self.host].all_tcp()) > 5:
            self.result['open_ports'] = 1
        else:
            self.result['open_ports'] = 0

    def openssh_ver(self):
        """OpenSSH version metric"""
        nm = nmap.PortScanner()

        (nm.scan(self.host.format(), str(self.port))).get('state')
        try:
            state = (nm[self.host]['tcp'][self.port]['state'])
            if state == "open":
                result = nm[self.host].tcp(self.port)
                if result['product'] == 'OpenSSH' and result['version'] in openssh_version:
                    self.result['openssh'] = 1
                else:
                    self.result['openssh'] = 0
            elif state == "closed":
                self.result['openssh'] = 0
                pass
        except KeyError as e:
            self.result['openssh'] = 0
            print(e)

    def uname_check(self):
        """ uname metric"""

        names = ['Linux svr04 3.2.0-4-amd64 #1 SMP Debian 3.2.68-1+deb7u1 x86_64 GNU/Linux',
                 'Linux station01 2.6.26-2-686 #1 SMP Wed Nov 4 20:45:37 UTC 2009 i686 GNU/Linux']

        ssh = self.create_ssh_agent()
        if ssh.connect() is not True:
            return

        uname = ssh.exec_command('uname -a')

        if uname[0] in names:
            self.result['uname'] = 1
        else:
            self.result['uname'] = 0

    @staticmethod
    def random_word(length):
        """ random string generator """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(length))
