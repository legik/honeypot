
from .ssh_agent import SshAgent


class Collector:
    """
        class Collector is a base class for metric collectors
    """

    def __init__(self, host):
        self.result = {}

        self.host = host[0]
        self.port = int(host[1])
        self.user_name = host[2]
        self.password = host[3]

        self.name = 'undefined'

    def create_ssh_agent(self):
        return SshAgent(self.host, self.port, self.user_name, self.password)

    def get_name(self):
        return self.name

    def run(self):
        raise NotImplementedError("Please Implement this method")
