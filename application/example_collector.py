
import logging
from .ssh_agent import SshAgent
from .collector import Collector


class ExampleCollector(Collector):
    """class ExampleCollector provides example class for calculating metrics"""
    def __init__(self):
        super().__init__()

        self.ssh = SshAgent('188.130.155.53', 2222, 'root', '')
        self.name = 'Test collector'

    def run(self):
        logging.info("Start TestCollector")

        out = self.ssh.exec_command('pwd')

        return [5,6,6]
