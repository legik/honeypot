
class MathModel:
    """class MathModel provides calculating final result"""

    def __init__(self, metrics):
        self.metrics = metrics
        self.result = {}
        self.coeff_base = {'create_file': 0.1, 'ping': 0.05, 'cd': 0.07,
                           'logpass': 0.05, 'VT_url': 0.11, 'return_value_cmd': 0.07,
                           'error_stream': 0.07, 'mkdir_with_flag': 0.07, 'inode': 0.07,
                           'loop': 0.07, 'wget': 0.07, 'open_ports': 0.1, 'openssh': 0.05, 'uname': 0.05}

    def calculate(self):
        if len(self.metrics) == 0:
            return 0

        self.result['Base'] = MathModel.multiply(self.coeff_base, self.metrics['Base'])

        return self.result

    @staticmethod
    def multiply(coeffs, metrics):
        for metr, coef in coeffs.items():
            if metr not in metrics.keys():
                continue

            metrics[metr] *= coef

        if 'prev_honey' in metrics and metrics['prev_honey'] == 1:
            return 1
        else:
            return sum(metrics.values())
