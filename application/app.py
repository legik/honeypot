import logging
from application.metrics_collector import MetricsCollector


# def setup_logger():
#     logger = logging.getLogger('Honeypot')
#     logger.setLevel(logging.DEBUG)
#
#     ch = logging.StreamHandler()
#     ch.setLevel(logging.DEBUG)
#
#     formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
#     ch.setFormatter(formatter)
#
#     logger.addHandler(ch)
#
#     pass


def start_application():
    """This function start application"""
    logging.info('Start application')
    # if not cli_args.collector:
    hosts = open('hosts', 'r')
    for h in hosts:
        if h[0] == '#':
            continue

        host = h.strip().split(':')
        print("IP address: {}:{}".format(host[0], host[1]))
        collect = MetricsCollector(host)
        collect.run()


if __name__ == '__main__':
    start_application()
